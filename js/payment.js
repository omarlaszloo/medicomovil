
$(document).ready(function() {
    OpenPay.setId('m2gq54nhmriuuxfz5lgb');
    OpenPay.setApiKey('pk_6710d2f3acba4bf58a224251fcf6de2f');
    OpenPay.setSandboxMode(true);


    $('#save-button').on('click', function(event) {
        event.preventDefault();
        $("#save-button").prop("disabled", true);
        OpenPay.token.extractFormAndCreate('customer-form', success_callbak, error_callbak);
    });

    var success_callbak = function(response) {
        var token_id = response.data.id;
        $('#token_id').val(token_id);
        $('#customer-form').submit();
    };

    var error_callbak = function(response) {
        var desc = response.data.description != undefined ? response.data.description : response.message;
        alert("ERROR [" + response.status + "] " + desc);
        $("#save-button").prop("disabled", false);
   };
})

