
/* validacion formulari */
$(document).ready(function() {
    $("form[name='form-payment']").validate({
        rules: {
            namePayment: "required",
            emailPayment: {
                required: true,
                email: true
            },
            expDatePayment: "required",
            cardPayment: "required",
            ccvPayment: "required"
        },
        
        messages: {
            namePayment: "Nombre requerido",
            emailPayment: "Email valido",
            cardPayment: "Numero requerido",
            expDatePayment: "Fecha requerido",
            ccvPayment: "ccv requerido"
        },

        submitHandler: function(form) {
            console.log(form)
            form.submit();
        }
    })      
})
